package com.example.soo_display_ui

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*

private const val REQUEST_ENABLE_BT_REQUEST_CODE = 1
private const val REQUEST_LOCATION_CODE = 2
const val EXTRA_MAC = "mac"
const val EXTRA_NAME = "dev"

class MainActivity : AppCompatActivity() {

    private lateinit var pairedDevices: ArrayList<BluetoothDevices>
    private lateinit var newDevices: ArrayList<BluetoothDevices>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) == PackageManager.PERMISSION_DENIED){
            Toast.makeText(applicationContext,
                "Missing permission 1", Toast.LENGTH_LONG).show()
        }
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN) == PackageManager.PERMISSION_DENIED){
            Toast.makeText(applicationContext,
                "Missing permission 2", Toast.LENGTH_LONG).show()
        }
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_DENIED){
            Toast.makeText(applicationContext,
                "Missing permission 3", Toast.LENGTH_LONG).show()
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION),
                REQUEST_LOCATION_CODE)
        }

        // Lookup the recyclerview in activity layout
        val rvPairedDevices = findViewById<View>(R.id.pairedDevices) as RecyclerView
        val rvNewDevices = findViewById<View>(R.id.newDevices) as RecyclerView

        // Initialize contacts
        pairedDevices = ArrayList<BluetoothDevices>()
        newDevices = ArrayList<BluetoothDevices>()
        // Create adapter passing in the sample user data
        val adapterPaired = PairedDevicesAdapter(pairedDevices)
        val adapterNew = NewDeviceAdapter(newDevices)
        // Attach the adapter to the recyclerview to populate items
        rvPairedDevices.adapter = adapterPaired
        rvNewDevices.adapter = adapterNew
        // Set layout manager to position the items
        rvPairedDevices.layoutManager = LinearLayoutManager(this)
        rvNewDevices.layoutManager = LinearLayoutManager(this)
        // That's all!

        val filter = IntentFilter()

        filter.addAction(BluetoothDevice.ACTION_FOUND)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED)
        filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED)

        registerReceiver(btEventsReceiver, filter)

        val bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if (bluetoothAdapter == null) {
            quitApplication("there's no bluetooth on this device")
        }
        if (!bluetoothAdapter.isEnabled) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT_REQUEST_CODE)
        }else{
            displayPairedDevices()
        }
        findViewById<Button>(R.id.buttonRefreshDevices).setOnClickListener {
            if(bluetoothAdapter.isDiscovering){
                bluetoothAdapter.cancelDiscovery()
                Toast.makeText(applicationContext,
                    "Stopping discovery", Toast.LENGTH_SHORT).show()
            }else{
                newDevices.clear()
                rvNewDevices.adapter?.notifyDataSetChanged()
                bluetoothAdapter.startDiscovery()
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            when(requestCode){
                REQUEST_ENABLE_BT_REQUEST_CODE ->{
                    if (resultCode == Activity.RESULT_OK) {
                        Toast.makeText(applicationContext,
                            "Bluetooth enabled", Toast.LENGTH_LONG).show()
                        displayPairedDevices()
                    } else {
                        quitApplication("it requires you to enable bluetooth")
                    }
                }
            }
        } catch (ex: Exception) {
            quitApplication("Unexpected exception: " + ex.message)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                             permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            REQUEST_LOCATION_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
                    quitApplication("permission is required")
                }
            }
        }
    }

    private fun quitApplication(why : String){
        val reason  = "Application closed because $why"
        println(reason)
        Toast.makeText(applicationContext, reason, Toast.LENGTH_LONG).show()
        finishAndRemoveTask()
    }

    private fun displayPairedDevices(){
        val rvPairedDevices = findViewById<View>(R.id.pairedDevices) as RecyclerView
        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()
        val pairedDevicesRetrieved: Set<BluetoothDevice>? = bluetoothAdapter?.bondedDevices
        pairedDevicesRetrieved?.forEach {device ->
            val newDev  = BluetoothDevices(device.name, device.address)
            pairedDevices.add(newDev)
            rvPairedDevices.adapter?.notifyItemInserted(pairedDevices.size - 1)
        }
    }

    // BroadcastReceiver triggered when a new BT device is found.
    private val btEventsReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            when(intent.action) {
                BluetoothDevice.ACTION_FOUND -> {
                    val rvNewDevices = findViewById<View>(R.id.newDevices) as RecyclerView
                    val device: BluetoothDevice? = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
                    newDevices.add(BluetoothDevices(device?.name ?: "Name unavailable", device?.address.toString()))
                    rvNewDevices.adapter?.notifyItemInserted(newDevices.size - 1)
                }
                BluetoothAdapter.ACTION_DISCOVERY_STARTED -> {
                    Toast.makeText(applicationContext,
                        "Discovery Started", Toast.LENGTH_SHORT).show()
                }
                BluetoothAdapter.ACTION_DISCOVERY_FINISHED -> {
                    Toast.makeText(applicationContext,
                        "Discovery Finished", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}

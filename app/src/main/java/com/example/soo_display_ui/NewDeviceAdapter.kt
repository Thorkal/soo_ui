package com.example.soo_display_ui

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView


// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
class NewDeviceAdapter (private val mDevices: List<BluetoothDevices>) : RecyclerView.Adapter<NewDeviceAdapter.ViewHolder>() {

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val deviceName : TextView = itemView.findViewById(R.id.device_name)
        val deviceMac : TextView = itemView.findViewById(R.id.device_mac)
    }



    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewDeviceAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val newDevicesView = inflater.inflate(R.layout.new_devices, parent, false)
        // Return a new holder instance
        return ViewHolder(newDevicesView)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mDevices.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Get the data model based on position
        val device: BluetoothDevices = mDevices[position]
        // Set item views based on your views and data model
        val pairedName = holder.deviceName
        pairedName.text = device.name
        val pairedMac = holder.deviceMac
        pairedMac.text = device.mac
        holder.itemView.setOnClickListener{

            val intent = Intent(holder.itemView.context, BluetoothActivity::class.java).apply {
                putExtra(EXTRA_MAC, device.mac)
                putExtra(EXTRA_NAME, device.name)
            }
            startActivity(holder.itemView.context, intent, null)
        }
    }
}
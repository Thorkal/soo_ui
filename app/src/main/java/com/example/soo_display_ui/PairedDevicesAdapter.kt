package com.example.soo_display_ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
class PairedDevicesAdapter (private val mDevices: List<BluetoothDevices>) : RecyclerView.Adapter<PairedDevicesAdapter.ViewHolder>() {

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(listItemView: View) : RecyclerView.ViewHolder(listItemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val deviceName : TextView = itemView.findViewById(R.id.device_name)
        val deviceMac : TextView = itemView.findViewById(R.id.device_mac)
    }

    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PairedDevicesAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val pairedDevicesView = inflater.inflate(R.layout.paired_devices, parent, false)
        // Return a new holder instance
        return ViewHolder(pairedDevicesView)
    }

    // Involves populating data into the item through holder
    override fun onBindViewHolder(viewHolder: PairedDevicesAdapter.ViewHolder, position: Int) {
        // Get the data model based on position
        val device: BluetoothDevices = mDevices[position]
        // Set item views based on your views and data model
        val pairedName = viewHolder.deviceName
        pairedName.text = device.name
        val pairedMac = viewHolder.deviceMac
        pairedMac.text = device.mac
        viewHolder.itemView.setOnClickListener{
            println("Paired device selected")
        }
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mDevices.size
    }
}
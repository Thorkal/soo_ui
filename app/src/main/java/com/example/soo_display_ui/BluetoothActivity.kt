package com.example.soo_display_ui

import android.bluetooth.BluetoothAdapter
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.util.*
import kotlin.collections.ArrayList

private val uuid: UUID = UUID.fromString("abcdefgh-ijkl-mnop-qrst-uvwxyz123456")

class BluetoothActivity : AppCompatActivity() {

    private var mac: String = ""
    private var name: String = ""
    private var badapter : BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

    private lateinit var mes: ArrayList<Me>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bluetooth)

        if(badapter.isDiscovering){
            badapter.cancelDiscovery()
        }

        mes = ArrayList()
        val rvMes = findViewById<View>(R.id.mesView) as RecyclerView
        val adapterMes = MeAdapter(mes)
        rvMes.adapter = adapterMes
        rvMes.layoutManager = LinearLayoutManager(this)

        fakeDisplay(9)

        val extras = intent.extras
        mac = extras?.get(EXTRA_MAC).toString()
        name = extras?.get(EXTRA_NAME).toString()

        findViewById<Button>(R.id.refreshmes).setOnClickListener{
            Toast.makeText(this, "Refreshing, be patient", Toast.LENGTH_SHORT).show()
            mes.clear()
            adapterMes.notifyDataSetChanged()
            fakeDisplay(5)
        }
    }

    private fun fakeDisplay(n : Int){
        val rvMes = findViewById<View>(R.id.mesView) as RecyclerView
        for(i in 1..n){
            mes.add(Me(i))
            rvMes.adapter?.notifyItemInserted(mes.size - 1)
        }
    }
}
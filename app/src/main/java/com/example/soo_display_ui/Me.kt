package com.example.soo_display_ui

class Me(val id: Int) {
    companion object {
        fun createArray(numMes: Int): ArrayList<Me> {
            val mes = ArrayList<Me>()
            for (i in 1..numMes) {
                mes.add(Me(i))
            }
            return mes
        }
    }
}
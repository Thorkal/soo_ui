package com.example.soo_display_ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView

class MeAdapter (private val mDevices: List<Me>) : RecyclerView.Adapter<MeAdapter.ViewHolder>() {

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        // Your holder should contain and initialize a member variable
        // for any view that will be set as you render a row
        val one : TextView = itemView.findViewById(R.id.one)
        val two : TextView = itemView.findViewById(R.id.two)
        val three : TextView = itemView.findViewById(R.id.three)
        val four : TextView = itemView.findViewById(R.id.four)
    }



    // ... constructor and member variables
    // Usually involves inflating a layout from XML and returning the holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeAdapter.ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        // Inflate the custom layout
        val meView = inflater.inflate(R.layout.me_layout, parent, false)
        // Return a new holder instance
        return ViewHolder(meView)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mDevices.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // Get the data model based on position
        val me: Me = mDevices[position]
        val one = holder.one
        one.text = "ME${me.id}"
        holder.itemView.setOnClickListener{
            Toast.makeText(holder.itemView.context,
                "ME${me.id} selected", Toast.LENGTH_SHORT).show()
        }
    }
}